cmake_minimum_required(VERSION 3.3)


set(TARGET voxl-tag-detector)
file(GLOB all_src_files "*.c" "*.cpp")
add_executable(${TARGET} ${all_src_files})
include_directories(
	/usr/include/opencv4/
)

# link against all possible opencv libs
file(GLOB OpenCV_LIBS "/usr/lib64/libopencv*.so")

target_link_libraries(${TARGET}
	${OpenCV_LIBS}
	/usr/lib64/libmodal_pipe.so
	/usr/lib64/libmodal_json.so
	/usr/lib64/libvoxl_cutils.so
	apriltag
)

install(
	TARGETS ${TARGET}
	LIBRARY			DESTINATION /usr/lib
	RUNTIME			DESTINATION /usr/bin
	PUBLIC_HEADER	DESTINATION /usr/include
)