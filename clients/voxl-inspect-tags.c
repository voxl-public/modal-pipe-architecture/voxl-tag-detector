/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>
#include <voxl_cutils.h>

#define PIPE_NAME			"tag_detections"
#define PROCESS_NAME		"voxl-inspect_tags"




/**
 * @brief      Convert a rotation matrix to a Tait-Bryan intrinsic XYZ (Roll
 *             Pitch Yaw) rotation sequence in degrees
 *
 *             Unlike the rc_math library which consistently uses the more
 *             common aerospace-standard ZYX Tait-Bryan rotation sequence in
 *             radians, this function uses the XYZ Tait-Bryan rotation sequence
 *             in degrees. The reason for this is that a primary use for the
 *             extrinsics config file is setting the IMU-to-Camera extrinsic
 *             relation which is most easily done in the field by observation in
 *             units of degrees instead of radians. Also, since the camera Z
 *             axis points out the lens, it is helpful for the last step in the
 *             rotation sequence to rotate the camera about its lens after
 *             getting rotating it to point in the right direction by Roll and
 *             Pitch.
 *
 *             Note that a rotation sequence that intrinsically rotates a
 *             coordinate frame A to align with coordinate frame B will convert
 *             to a rotation matrix that rotates a vector from being represented
 *             in frame B to frame A.
 *
 * @param[in]  R     Rotation Matrix
 * @param[out] tb    resulting Tait-Bryan intrinsic XYZ (Roll Pitch Yaw)
 *                   rotation sequence in degrees
 */
static void _rotation_matrix_to_tait_bryan_xyz_degrees(float R[3][3], float* roll, float* pitch, float* yaw)
{
	#ifndef RAD_TO_DEG
	#define RAD_TO_DEG (180.0/M_PI)
	#endif

	*pitch = asin(R[0][2])*RAD_TO_DEG;
	if(fabs(R[0][2]) < 0.9999999){
		*roll = atan2(-R[1][2], R[2][2])*RAD_TO_DEG;
		*yaw  = atan2(-R[0][1], R[0][0])*RAD_TO_DEG;
	}
	else{
		*roll = atan2(-R[2][1], R[1][1])*RAD_TO_DEG;
		*yaw  = 0.0f;
	}
	return;
}


// called whenever we connect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf(CLEAR_TERMINAL DISABLE_WRAP RESET_FONT);
	printf("connected to voxl-tag-detector");
	return;
}

// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "server disconnected");
	return;
}



static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int n_packets, i;
	tag_detection_t* data_array = pipe_validate_tag_detection_t(data, bytes, &n_packets);

	// error check
	if(data_array == NULL) return;

	// print all detections
	for(i=0;i<n_packets;i++){

		tag_detection_t d = data_array[i];
		float roll, pitch, yaw;
		_rotation_matrix_to_tait_bryan_xyz_degrees(d.R_tag_to_cam, &roll, &pitch, &yaw);

		int64_t now = VCU_time_monotonic_ns();
		double latency_ms = (now - d.timestamp_ns)/1000000.0;

		printf("\n");
		printf("id:%3d name: %s\n", d.id, d.name);
		printf("XYZ: %6.2f %6.2f %6.2f \n", (double)d.T_tag_wrt_cam[0], (double)d.T_tag_wrt_cam[1], (double)d.T_tag_wrt_cam[2]);
		printf("RPY: %6.1f %6.1f %6.1f \n", (double)roll, (double)pitch, (double)yaw);
		printf("size_m: %4.2f latency_ms: %6.2f\n", (double)d.size_m, latency_ms);
		printf("cam: %s type: %s\n", d.cam, pipe_tag_location_type_to_string(d.loc_type));
		printf("\n");
	}
	return;
}


int main()
{
	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// set up all our MPA callbacks
	pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// open the pipe
	printf(CLEAR_TERMINAL "waiting for server at %s\n", PIPE_NAME);
	int ret = pipe_client_open(0, PIPE_NAME, PROCESS_NAME, \
									CLIENT_FLAG_EN_SIMPLE_HELPER, \
									TAG_DETECTION_RECOMMENDED_READ_BUF_SIZE);

	// check for errors trying to connect to the server pipe
	if(ret<0){
		pipe_print_error(ret);
		printf(ENABLE_WRAP);
		return -1;
	}

	// keep going until the  signal handler sets the running flag to 0
	while(main_running){
		usleep(500000);
	}

	// all done, signal pipe read threads to stop
	printf("\nclosing and exiting\n" ENABLE_WRAP);
	pipe_client_close_all();

	return 0;
}